function PrintBill() {
    let menu = document.getElementById("get-menu");
    menu.innerHTML = "";
    axios({
        method:"GET",
        url:  "https://desingpatternsproject1-default-rtdb.europe-west1.firebasedatabase.app/Cart/" +".json",
      })
    .then(function(response) {
        var totalVat =0;
        var totalAmmount = 0;
      for (const [key, value1] of Object.entries(response.data)) {
        let ulItem = document.createElement("ul");
        let element = document.createElement("li");
        element.classList.add("list-group-item", "d-flex","align-items-center" , "justify-content-between")
        
        let name = document.createElement("div");
        name.innerHTML= value1.name;
        let price = document.createElement("div");
        price.innerHTML= Number(value1.price) + value1.price* value1.vat + "RON";
         totalVat = totalVat + value1.price*value1.vat;
        totalAmmount = totalAmmount + Number(value1.price);
        let element2 = document.createElement("li");
        element2.classList.add("list-group-item", "d-flex","align-items-center" , "justify-content-between")

        let ingredients = document.createElement("div");
        ingredients.innerHTML = value1.ingridients;
        menu.appendChild(ulItem);
        ulItem.appendChild(element);
        ulItem.appendChild(element2);
        element.appendChild(name);
        element.appendChild(price);
        element2.appendChild(ingredients);
        
        let element3 = document.createElement("br");
        menu.appendChild(element3);
    }
    let Vat = document.createElement("li");
        Vat.classList.add("list-group-item", "d-flex","align-items-center" , "justify-content-between")
        Vat.innerHTML ="Total vat: "+ totalVat + " RON, Total price: " + Number(totalAmmount+ totalVat) ;
    menu.appendChild(Vat);
    })
}
PrintBill();


