function getFoodItem() {
    let menu = document.getElementById("get-menu");
    menu.innerHTML = "";
    axios({
        method:"GET",
        url:  "https://desingpatternsproject1-default-rtdb.europe-west1.firebasedatabase.app/Menu/" +".json",
      })
    .then(function(response) {
      for (const [key, value1] of Object.entries(response.data)) {
        let ulItem = document.createElement("ul");
        let h5Item = document.createElement("h5");
        h5Item.classList.add("list-group-item", "active")
        menu.appendChild(ulItem);
        ulItem.appendChild(h5Item);
        h5Item.innerHTML= key ;
      for (const [key, value] of Object.entries(value1)) {
        let element = document.createElement("li");
        element.classList.add("list-group-item", "d-flex","align-items-center" , "justify-content-between")
        let nameDiv = document.createElement("div");
        let priceDiv = document.createElement("div");
        let cart = document.createElement("button");
        cart.onclick = ()=>{
            addToCart(value);
        }
        cart.classList.add("btn", "btn-danger");
        cart.innerHTML="Add to cart";
        nameDiv.innerHTML= value.name;
        priceWithVAT = Number(value.price) +  value.price* value.vat;
        priceDiv.innerHTML=  priceWithVAT + " RON";
        ulItem.appendChild(element);
        element.appendChild(nameDiv);
        element.appendChild(priceDiv);
        element.appendChild(cart);
      }
      let element3 = document.createElement("br");
      menu.appendChild(element3);
    }
    })
}
getFoodItem();


function addToCart(item){
    axios({
        method: "post",
        url: "https://desingpatternsproject1-default-rtdb.europe-west1.firebasedatabase.app/Cart.json",
        data: item
    })
}