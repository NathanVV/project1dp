class Pizza {
    constructor(options) {

        this.name = options.name;
        this.price = options.price;
        this.ingridients = options.ingridients;
        this.vat = options.vat;
    }
}
   
  class Pasta {
    constructor(options) {

        this.name = options.name ;
        this.price = options.price;
        this.ingridients = options.ingridients;
        this.vat = options.vat;
    }
}
  class Meat {
    constructor(options) {

        this.name = options.name ;
        this.price = options.price;
        this.ingridients = options.ingridients || "Normal";
        this.vat =options.vat;
    }
}
  class SideDish {
    constructor(options) {
        this.name = options.name ;
        this.price = options.price;
        this.vat = options.vat;
    }
}
  class Dressing {
    constructor(options) {
        this.name = options.name ;
        this.price = options.price;
        this.vat = options.vat;
    }
}
   
  class Desert {
    constructor(options) {
        this.name = options.name ;
        this.price = options.price;
        this.vat = options.vat;
    }
}
  class Soda {
    constructor(options) {
        this.name = options.name ;
        this.price = options.price;
        this.vat = options.vat;
    }
}
  class NaturalDrink {
    constructor(options) {
        this.name = options.name ;
        this.price = options.price;
        this.vat = options.vat;
    }
}
   
   
   
  function MenuFactory() {}
   
   
   
  MenuFactory.prototype.createMenu = function ( options ) {
   
    switch(options.menuType){
      case "pizza":
        this.menuClass = Pizza;
        break;
      case "pasta":
        this.menuClass = Pasta;
        break;
      case "meat":
        this.menuClass = Meat;
        break;
      case "sideDish":
        this.menuClass = SideDish;
        break;
      case "dressing":
        this.menuClass = Dressing;
        break;
      case "desert":
        this.menuClass = Desert;
        break;
      case "soda":
        this.menuClass = Soda;
        break;
      case "naturalDrink":
        this.menuClass = NaturalDrink;
        break;
    }
   
    return new this.menuClass( options );
   
  };
   
    

    function createPizza(){
        var inputName, inputPrice, inputIngridients;
        
        var pizzaFactory = new MenuFactory();
    MenuFactory.prototype.menuClass = Pizza;
    inputName = document.getElementById("name").value;
    inputPrice = document.getElementById("price").value;
    inputIngridients = document.getElementById("ingridients").value;
    
    var pizza= pizzaFactory.createMenu({
          name: inputName,
          price: inputPrice,
          ingridients: inputIngridients,
          vat: 0.09
        })
        addFood(pizza);
    }

    function createPasta(){
        var inputName, inputPrice, inputIngridients;
        
        var pastaFactory = new MenuFactory();
    MenuFactory.prototype.menuClass = Pasta;
    inputName = document.getElementById("name").value;
    inputPrice = document.getElementById("price").value;
    inputIngridients = document.getElementById("ingridients").value;
    
    var pasta= pastaFactory.createMenu({
          name: inputName,
          price: inputPrice,
          ingridients: inputIngridients,
          vat:0.09
        })
        addFood(pasta);
    }

    function createMeat(){
        var inputName, inputPrice, inputIngridients;
        
        var meatFactory = new MenuFactory();
    MenuFactory.prototype.menuClass = Meat;
    inputName = document.getElementById("name").value;
    inputPrice = document.getElementById("price").value;
    inputIngridients = document.getElementById("ingridients").value;
    
    var meat= meatFactory.createMenu({
          name: inputName,
          price: inputPrice,
          rarety: inputIngridients,
          vat: 0.09
        })
        addFood(meat);
    }

    function createSideDish(){
        var inputName, inputPrice;
        
        var sideDishFactory = new MenuFactory();
    MenuFactory.prototype.menuClass = SideDish;
    inputName = document.getElementById("name").value;
    inputPrice = document.getElementById("price").value;
    
    var sideDish= sideDishFactory.createMenu({
          name: inputName,
          price: inputPrice,
          vat: 0.09
        })
        addFood(sideDish);
    }

    function createDressing(){
        var inputName, inputPrice;
        
        var dressingFactory = new MenuFactory();
    MenuFactory.prototype.menuClass = Dressing;
    inputName = document.getElementById("name").value;
    inputPrice = document.getElementById("price").value;
    
    var dressing= dressingFactory.createMenu({
          name: inputName,
          price: inputPrice,
          vat: 0.09
        })
        addFood(dressing);
    }

    function createDesert(){
        var inputName, inputPrice;
        
        var desertFactory = new MenuFactory();
    MenuFactory.prototype.menuClass = Desert;
    inputName = document.getElementById("name").value;
    inputPrice = document.getElementById("price").value;
    
    var desert= desertFactory.createMenu({
          name: inputName,
          price: inputPrice,
          vat: 0.10
        })
        addFood(desert);
    }
    function createSoda(){
        var inputName, inputPrice;
        
        var sodaFactory = new MenuFactory();
    MenuFactory.prototype.menuClass = Soda;
    inputName = document.getElementById("name").value;
    inputPrice = document.getElementById("price").value;
    
    var soda= sodaFactory.createMenu({
          name: inputName,
          price: inputPrice,
          vat: 0.12
        })
        addFood(soda);
    }

    function createNaturalDrink(){
        var inputName, inputPrice;
        
        var naturalDrinkFactory = new MenuFactory();
    MenuFactory.prototype.menuClass = NaturalDrink;
    inputName = document.getElementById("name").value;
    inputPrice = document.getElementById("price").value;
    
    var naturalDrink= naturalDrinkFactory.createMenu({
          name: inputName,
          price: inputPrice,
          vat: 0.05
        })
        addFood(naturalDrink);
    }


       function addFood(item) {
            axios({
                method: "post",
                url: "https://desingpatternsproject1-default-rtdb.europe-west1.firebasedatabase.app/Menu/" +  MenuFactory.prototype.menuClass.name +".json",
                data: item
            })

        }

     

        
     

  

      
