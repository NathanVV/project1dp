function getFoodItem() {
  var totalAmmount = 0;
    let menu = document.getElementById("get-menu");
    menu.innerHTML = "";
    axios({
        method:"GET",
        url:  "https://desingpatternsproject1-default-rtdb.europe-west1.firebasedatabase.app/Cart/" +".json",
      })
    .then(function(response) {
      for (const [key, value1] of Object.entries(response.data)) {
        let ulItem = document.createElement("ul");
        let element = document.createElement("li");
        element.classList.add("list-group-item", "d-flex","align-items-center" , "justify-content-between")
        
        let name = document.createElement("div");
        name.innerHTML= value1.name;
        let price = document.createElement("div");
        price.innerHTML= Number(value1.price) + value1.price* value1.vat + "RON";
        let button = document.createElement("button");
        button.classList.add("btn", "btn-secondary");
        button.innerHTML= "Remove item";
        button.onclick = ()=>{
            
            deleteFromCart(key);
        }
        let element2 = document.createElement("li");
        element2.classList.add("list-group-item", "d-flex","align-items-center" , "justify-content-between")

        let ingredients = document.createElement("div");
        ingredients.innerHTML = value1.ingridients;

        totalAmmount = totalAmmount +Number(value1.price) + value1.price* value1.vat;
        menu.appendChild(ulItem);
        ulItem.appendChild(element);
        ulItem.appendChild(element2);
        element.appendChild(name);
        element.appendChild(price);
        element2.appendChild(ingredients);
        element.appendChild(button);

      let element3 = document.createElement("br");
      menu.appendChild(element3);
    }
    let totalPrice = document.createElement("ul");
    totalPrice.innerHTML= "Total price: " + totalAmmount + " RON";
    menu.appendChild(totalPrice);

    })
}
getFoodItem();


function deleteFromCart(item){
    console.log(item);
    axios({
        method: "delete",
        url: "https://desingpatternsproject1-default-rtdb.europe-west1.firebasedatabase.app/Cart/" + item + ".json",
    })
}